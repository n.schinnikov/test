FROM ubuntu:20.04

RUN mkdir /install
COPY ["./*requirements*", "./Makefile", "/install/"]

#Переменная нужна чтобы nginx при установке не запрашивал timezone
ENV DEBIAN_FRONTEND=noninteractive

RUN cd /install && \ 
    apt update && \
    apt install -y nginx python3.8 python3-pip && \
    pip install --upgrade pip pip-tools && \
    make && \ 
    pip install uwsgi

COPY . /app

CMD ["bash"]
