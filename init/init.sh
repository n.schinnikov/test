#!/bin/bash
service nginx start
cd /app/src
python3 manage.py migrate
#Создаем суперпользователя django
echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('$DJANGO_SU_NAME', '', '$DJANGO_SU_PASSWORD')" | python3 manage.py shell
#Копируем конфиги nginx
cp /app/init/uwsgi.params /etc/nginx/
cp /app/init/uwsgipass.conf /etc/nginx/conf.d/
nginx -s reload
#Копируем uwsgi.ini
cp /app/init/uwsgi.ini /app/
uwsgi --ini /app/uwsgi.ini

#для запуска в режиме демона
#tail -f /dev/null
#mkdir -p /var/log/uwsgi/app/
#touch /var/log/uwsgi/app/uwsgi.log